<?php

	include  'application/core/controller.php';
    include  'application/core/view.php';
    include  'application/core/model.php';

    class Route {
        public function __construct() {
			if(!isset($_SESSION)){
                session_start();
            }
        }

        public function start() {
            $controller_name = 'main';
            $action_name = 'index';

            $routes = explode('/', $_SERVER['REQUEST_URI']);

            // получение имени контроллера
            if($routes[1] != '') {
                $controller_name = $routes[1];
            }

            // получениме имени action
            if($routes[2] != '') {
                $action_name = $routes[2];
            }

            $model_name = 'Model_'.$controller_name;
            $controller_name = 'Controller_'.$controller_name;
            $action_name = 'action_'.$action_name;

            // подцепляем файил с классом модели
            $model_file = strtolower($model_name).'.php';
            $model_path = 'application/models/'.$model_file;
            if(file_exists($model_path)) {
                include 'application/models/'.$model_file;
            }

            // подцепляем фаил с классом контроллера
            $controller_file = strtolower($controller_name).'.php';
            $controller_path = 'application/controllers/'.$controller_file;

            if(file_exists($controller_path)) {
                include 'application/controllers/'.$controller_file;
            } else {
                $this->ErrorPage404();
            }

            // создание контроллера
            $controller = new $controller_name;
            $action = $action_name;

            if(method_exists($controller, $action)) {
                $controller->$action();
            } else {
                $this->ErrorPage404();
            }
        }

        function ErrorPage404() {
            $host = 'http://'.$_SERVER['HTTP_HOST'].'/';
            header('HTTP/1.1 404 Not Found');
            header("Status: 404 Not Found"); var_dump($host);
            header('Location:'.$host.'404');
        }
    }
?>