<?php
    require_once 'configuration.php';
    require_once 'url.php';

    class Controller {
        public $model;
        public $view;
        public $url;
        public $configuration;

        public function __construct() {
            $this->configuration = new Configuration();
            $this->url = new URL();

            $this->configuration->add_css('/css/bootstrap.min.css');
            //$this->configuration->add_css('css/bootstrap-theme.min.css');
           //$this->add_css('../css/bootstrap-reboot.min.css');
            //$this->configuration->add_css('../css/style.css');
           //$this->add_js('../js/bootstrap.min.js');
           //$this->add_js('../js/bootstrap.bundle.min.js');
           //$this->configuration->add_js('/js/jquery.min.js');
           //$this->add_js('../js/script.js');
        }

        public function generate_js_script() {
            $content = "";
            foreach ($this->configuration->get_js() as $item) {
                $content .= "<script src='{$item}' type='text/javascript'></script>";
            }
            return $content;
        }

        public function generate_css_style() {
            $content = "";
            foreach($this->configuration->get_css() as $item) {
                $content .= "<link href='{$item}' rel='stylesheet' type='text/css'>";
            }
            return $content;
        }

        public function name_controller() {
            $routes = explode('/', $_SERVER['REQUEST_URI']);
            if($routes[1] == ''){
                $routes[1] = 'main';
                return $routes[1];
            }
            return $routes[1];
        }

        public function get_configuration(){
            return $this->configuration;
        }

        public function get_url(){
            return $this->url;
        }
    }
