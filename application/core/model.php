<?php
class Model {
    var $host = 'localhost';
    var $user = 'admin';
    var $password = 'admin';
    var $database = 'framework';

    var $link;

    public function __construct() {
        $this->link = mysqli_connect($this->host, $this->user, $this->password, $this->database);
    }

    function getLink() {
        return $this->link;
    }
}
