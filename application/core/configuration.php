<?php
    class Configuration{
        public $array_js = array();
        public $array_css = array();
        public $array_fonts = array();

        public function add_js($js_script) {
            array_push($this->array_js, $js_script);
        }

        public function add_css($css_script) {
            array_push($this->array_css, $css_script);
        }

        public function add_font($font_script) {
            array_push($this->array_fonts, $font_script);
        }

        public function get_js() {
            return $this->array_js;
        }

        public function get_css() {
            return $this->array_css;
        }

        public function get_font() {
            return $this->array_fonts;
        }

    }
