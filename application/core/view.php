<?php
    class View{
        private $controller;
        public function __construct($controller){
            $this->controller = $controller;
        }

        public function render($content_file, $template_file, $data = null) {
            $controller = $this->controller;
            include 'application/views/'.$template_file;
        }
    }
