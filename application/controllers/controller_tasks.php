<?php
    class Controller_tasks extends Controller{
        public function __construct(){
            parent::__construct();
            $this->view = new View($this);
        }

        public function action_tasks(){
            $this->view->render('tasks_view.php','template_file.php');
        }

        public function action_page(){
            $this->view->render('page_view.php','template_file.php');
        }
    }
