<?php
    class Controller_main extends Controller {

        public function __construct(){
            parent::__construct();

            $this->model = new Model_main($this);
            $this->view = new View($this);

        }

        public function action_index(){
            $data = $this->model->getTasks();
            $this->view->render('main_view.php','template_file.php', $data);
        }

        public function action_registration(){
            $login_reg = filter_input(INPUT_POST, 'login_reg');
            $email_reg = filter_input(INPUT_POST, 'email_reg');
            $password_reg = filter_input(INPUT_POST, 'password_reg');
            $status_reg = filter_input(INPUT_POST, 'status_reg');

            if($login_reg != '' && $email_reg != '' & $password_reg != '') {
                $password_hash = password_hash($password_reg, PASSWORD_DEFAULT);
                if($this->model->insert_users($login_reg, $email_reg, $password_hash, $status_reg)) {
                    $this->action_login();
                    return;
                }
            }
            $this->view->render('registration_view.php','template_file.php');
        }

        public function action_login(){
            $login_log = filter_input(INPUT_POST, login_log);
            $password_log = filter_input(INPUT_POST, password_log);
            if($login_log != '' ){
                if($this->model->get_user($login_log, $password_log)){
                    $this->action_index();
                    return;
                } else{
                    $this->view->render('login_view.php','template_file.php');
                    return;
                }
            }
            $this->view->render('login_view.php','template_file.php');
        }

        public function action_exit(){
            $this->view->render('exit_view.php','template_file.php');
        }


    }
