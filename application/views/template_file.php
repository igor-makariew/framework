<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <title></title>
    <?= $controller->generate_css_style();?>
</head>
<body>
<nav class="nav nav-pills nav-justified">
    <a class="nav-item nav-link <?= $controller->url->is_current_this_page("index")?"active":"" ?>" href="<?php echo '/main/index'?>">Главная</a>
    <a class="nav-item nav-link <?= $controller->url->is_current_this_page("tasks")?"active":"" ?>" href="<?php echo '/tasks/tasks'?>">Задачи</a>
    <a class="nav-item nav-link <?= $controller->url->is_current_this_page("page")?"active":"" ?>" href="<?php echo '/tasks/page'?>">Link</a>
    <?php if($_SESSION['login']):?>
        <a class="nav-item nav-link <?= $controller->url->is_current_this_page("exit")?"active":"" ?>" href="<?php echo '/main/exit'?>">Выйти</a>
    <?php else:?>
        <a class="nav-item nav-link <?= $controller->url->is_current_this_page("login")?"active":"" ?>" href="<?php echo '/main/login'?>">Авторизация</a>
    <?php endif;?>
</nav>
<?php if($_SESSION['login']):?>
    <div class="alert alert-primary" role="alert">
        вы залогинены
    </div>
<?php else:?>
    <div class="alert alert-secondary" role="alert">
        вы гость
    </div>
<?php endif;?>
<?php if($_SESSION['user'][0]['login'] != ''):?>
    <div class="alert alert-primary" role="alert">
        Имя пользователя: <?= $_SESSION['user'][0]['login'] ?>
    </div>
<?php else:?>
    <div class="alert alert-secondary" role="alert">
        Гость
    </div>
<?php endif;?>
<div class="container">
    <?php include 'application/views/'.$content_file; ?>
</div>
<?= $controller->generate_js_script();?>
</body>
</html>.