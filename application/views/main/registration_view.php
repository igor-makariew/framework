<h3>Registration</h3>
<form action="../main/registration" method="post">
    <div class="form-group">
        <label for="exampleInputLogin">Login</label>
        <input type="text" class="form-control" id="exampleInputLogin" aria-describedby="emailHelp" name="login_reg">
    </div>
    <div class="form-group">
        <label for="exampleInputEmail1">Email address</label>
        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="email_reg">
        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
    </div>
    <div class="form-group">
        <label for="exampleInputPassword1">Password</label>
        <input type="password" class="form-control" id="exampleInputPassword1" name="password_reg">
    </div>
    <input type="hidden" class="form-control" id="exampleInputStatus1" name="status_reg" value="10">
<!--    <div class="form-group form-check">-->
<!--        <input type="checkbox" class="form-check-input" id="exampleCheck1">-->
<!--        <label class="form-check-label" for="exampleCheck1">Check me out</label>-->
<!--    </div>-->
    <button type="submit" class="btn btn-primary">Registration</button>
</form>
